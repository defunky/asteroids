#ifndef __HIGHSCORE_H__
#define __HIGHSCORE_H__

#include <fstream>
#include <vector>
#include <tuple>

class Highscores
{
public:
	Highscores();
	void AddNewHighscore(std::string playerName, int highscore);
	std::string GetAllHighscores();
	bool IsCurrentScoreHighscore(int score);

private:
	void InitalizeHighscoreFile();
	void SaveHighscores();


	const std::string HS_FILE = "highscores.txt";
	const int HIGHSCORE_LIMIT = 10;

	std::ifstream mFile;
	std::vector<std::tuple<std::string, int>> mHighscores;


};
#endif
