#include "Asteroid.h"
#include "Asteroids.h"
#include "Animation.h"
#include "AnimationManager.h"
#include "GameUtil.h"
#include "GameWindow.h"
#include "GameWorld.h"
#include "GameDisplay.h"
#include "Spaceship.h"
#include "BoundingShape.h"
#include "BoundingSphere.h"
#include "GUILabel.h"
#include "Explosion.h"

// PUBLIC INSTANCE CONSTRUCTORS ///////////////////////////////////////////////

/** Constructor. Takes arguments from command line, just in case. */
Asteroids::Asteroids(int argc, char *argv[])
	: GameSession(argc, argv)
{
	mLevel = 0;
	mObjectiveCount = 0;
	mIsTypingName = false;
}

/** Destructor. */
Asteroids::~Asteroids(void)
{
}

// PUBLIC INSTANCE METHODS ////////////////////////////////////////////////////

/** Start an asteroids game. */
void Asteroids::Start()
{
	// Create a shared pointer for the Asteroids game object - DO NOT REMOVE
	shared_ptr<Asteroids> thisPtr = shared_ptr<Asteroids>(this);

	// Add this class as a listener of the game world
	mGameWorld->AddListener(thisPtr.get());

	// Add this as a listener to the world and the keyboard
	mGameWindow->AddKeyboardListener(thisPtr);

	// Add a score keeper to the game world
	mGameWorld->AddListener(&mScoreKeeper);

	// Add this class as a listener of the score keeper
	mScoreKeeper.AddListener(thisPtr);

	// Create an ambient light to show sprite textures
	GLfloat ambient_light[] = { 1.0f, 1.0f, 1.0f, 1.0f };
	GLfloat diffuse_light[] = { 1.0f, 1.0f, 1.0f, 1.0f };
	glLightfv(GL_LIGHT0, GL_AMBIENT, ambient_light);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, diffuse_light);
	glEnable(GL_LIGHT0);

	Animation *explosion_anim = AnimationManager::GetInstance().CreateAnimationFromFile("explosion", 64, 1024, 64, 64, "explosion_fs.png");
	Animation *asteroid1_anim = AnimationManager::GetInstance().CreateAnimationFromFile("asteroid1", 128, 8192, 128, 128, "asteroid1_fs.png");
	Animation *spaceship_anim = AnimationManager::GetInstance().CreateAnimationFromFile("spaceship", 128, 128, 128, 128, "spaceship_fs.png");
	Animation *enemy_anim = AnimationManager::GetInstance().CreateAnimationFromFile("enemy", 128, 8192, 128, 128, "enemy_fs.png");

	// Create a spaceship and add it to the world
	mGameWorld->AddObject(CreateSpaceship());

	// Create some asteroids and add them to the world
	CreateAsteroids(5);
	//Create spaceship enemy
	CreateEnemyship();

	//Create the GUI
	CreateGUI();

	// Add a player (watcher) to the game world
	mGameWorld->AddListener(&mPlayer);

	// Add this class as a listener of the player
	mPlayer.AddListener(thisPtr);

	//Don't listen to player name typing yet
	mIsTypingName = false;

	//Add listener, as alien must know player bullet pos
	mGameWorld->AddListener(mAlien.get());

	// Start the game
	GameSession::Start();
}

/** Stop the current game. */
void Asteroids::Stop()
{
	// Stop the game
	GameSession::Stop();
}

// PUBLIC INSTANCE METHODS IMPLEMENTING IKeyboardListener /////////////////////

void Asteroids::OnKeyPressed(uchar key, int x, int y)
{
	if(mIsTypingName){
		LeaderboardNameInput(key, x, y); //Listen for keys when typing in leaderboard name
	}
	else {
		InGameInput(key, x, y); //Listen for keys when ingame.
	}
}

void Asteroids::InGameInput(uchar key, int x, int y)
{
	switch (key)
	{
	case ' ':
		mSpaceship->Shoot();
		break;
	default:
		break;
	}
}

void Asteroids::LeaderboardNameInput(uchar key, int x, int y)
{
	if((key > 64 && key < 91 || key > 96 && key < 123) //ASCII [A-Z] 65-90, [a-z] 97-122
		&& mPlayer.GetName().size() < 15){  //Limit name to 15 chars
		mPlayer.AppendCharToName(key);
	}else if(key == '\b')  // Backspace to remove last character.
	{
		if (mPlayer.GetName().size() > 0) mPlayer.DeleteLastCharFromName();
	}

	mNameLabel->SetText(mPlayer.GetName());

	if(key == '\r')
	{
		mIsTypingName = false;
		//Add the highscore, to update highscore file 
		mHighscores.AddNewHighscore(mPlayer.GetName(), mScoreKeeper.GetScore());
		mLeaderboard->SetText(mHighscores.GetAllHighscores()); //Update the leaderboard
		SetTimer(250, SHOW_LEADERBOARD);
	}
}

void Asteroids::OnKeyReleased(uchar key, int x, int y) {}

void Asteroids::OnSpecialKeyPressed(int key, int x, int y)
{
	switch (key)
	{
	// If up arrow key is pressed start applying forward thrust
	case GLUT_KEY_UP: mSpaceship->Thrust(10); break;
	// If left arrow key is pressed start rotating anti-clockwise
	case GLUT_KEY_LEFT: mSpaceship->Rotate(90); break;
	// If right arrow key is pressed start rotating clockwise
	case GLUT_KEY_RIGHT: mSpaceship->Rotate(-90); break;
	// Default case - do nothing
	default: break;
	}
}

void Asteroids::OnSpecialKeyReleased(int key, int x, int y)
{
	switch (key)
	{
	// If up arrow key is released stop applying forward thrust
	case GLUT_KEY_UP: mSpaceship->Thrust(0); break;
	// If left arrow key is released stop rotating
	case GLUT_KEY_LEFT: mSpaceship->Rotate(0); break;
	// If right arrow key is released stop rotating
	case GLUT_KEY_RIGHT: mSpaceship->Rotate(0); break;
	// Default case - do nothing
	default: break;
	} 
}


// PUBLIC INSTANCE METHODS IMPLEMENTING IGameWorldListener ////////////////////

void Asteroids::OnObjectRemoved(GameWorld* world, shared_ptr<GameObject> object)
{
	if (object->GetType() == GameObjectType("Asteroid"))
	{
		auto brokenAsteroid = static_pointer_cast<Asteroid>(object); //We cast it to get access to Asteroid methods
		AsteroidSize currentSize = brokenAsteroid->GetCurrentSize(); //We use the hit count as a scale factor
		GLVector3f roidPosition = brokenAsteroid->GetPosition(); //We use the position to generate smaller asteroids

		shared_ptr<GameObject> explosion = CreateExplosion(1.0f/currentSize); //Size is implicity converted to int, scale explosion with size
		explosion->SetPosition(object->GetPosition());
		explosion->SetRotation(object->GetRotation());
		mGameWorld->AddObject(explosion);
		mObjectiveCount--;

		if (currentSize < Tiny) // We limit the chain to 3 asteroids when splitting. Big -> Small -> Tiny -> No more.
		{
			AsteroidSize newSize = static_cast<AsteroidSize>(currentSize+1); //Change the new size to next size in the enum list
			CreateAsteroids(2, newSize, &roidPosition); //Pass in previous position of broken asteroid to set it correctly
		}  

		if (mObjectiveCount <= 0)
		{
			SetTimer(500, START_NEXT_LEVEL);
		}

	}else if(object->GetType() == GameObjectType("EnemyShip"))
	{
		shared_ptr<GameObject> explosion = CreateExplosion(0.5f);
		explosion->SetPosition(object->GetPosition());
		explosion->SetRotation(object->GetRotation());
		mGameWorld->AddObject(explosion);
		mObjectiveCount--;

		if (mObjectiveCount <= 0)
		{
			SetTimer(500, START_NEXT_LEVEL);
		}
	}
}

// PUBLIC INSTANCE METHODS IMPLEMENTING ITimerListener ////////////////////////

void Asteroids::OnTimer(int value)
{
	if (value == CREATE_NEW_PLAYER)
	{
		mSpaceship->Reset();
		mGameWorld->AddObject(mSpaceship);
	}

	if (value == START_NEXT_LEVEL)
	{
		mLevel++;
		int num_asteroids = 5 + 2 * mLevel;
		CreateAsteroids(num_asteroids);

		//Create an alien spaceship
		CreateEnemyship();
	}

	if (value == SHOW_GAME_OVER)
	{
		mGameOverLabel->SetVisible(true);
		if(mHighscores.IsCurrentScoreHighscore(mScoreKeeper.GetScore()))
		{
			mCongratsLabel->SetVisible(true);
			mNameLabel->SetVisible(true);
			mIsTypingName = true;
		}else
		{
			SetTimer(2000, SHOW_LEADERBOARD);
		}
	}

	if(value == SHOW_LEADERBOARD)
	{   
		//Hide Gameover/Nameinput GUI
		mNameLabel->SetVisible(false);
		mCongratsLabel->SetVisible(false);
		mGameOverLabel->SetVisible(false);

		//Display leaderboard
		mLeaderboard->SetVisible(true);
	}

	if(value == ATTACK_PLAYER)
	{
		//If alien is not dodging shoot the player
		if(!mAlien->IsDodging()) mAlien->Shoot();
		//Set a random timer for 3.5 to shoot the player
		SetTimer(rand() % 3500, ATTACK_PLAYER);
	}

	if(value == DODGE_PLAYER)
	{
		//Calculate whether or not we're in a dodge state
		mAlien->CalculateDodge();
		//If we are try to dodge.
		if(mAlien->IsDodging())
		{
			//If we dodge rotate the ship and add a thrust
			mAlien->AddAngle(90);
			mAlien->Thrust(35);
			//Wait 1.2s before dodging again
			SetTimer(1200, DODGE_PLAYER);

		}else
		{
			//0.1s before dodging again
			SetTimer(100, DODGE_PLAYER);
		}
	}

}

// PROTECTED INSTANCE METHODS /////////////////////////////////////////////////
shared_ptr<GameObject> Asteroids::CreateSpaceship()
{
	// Create a raw pointer to a spaceship that can be converted to
	// shared_ptrs of different types because GameWorld implements IRefCount
	mSpaceship = make_shared<Spaceship>();
	mSpaceship->SetBoundingShape(make_shared<BoundingSphere>(mSpaceship->GetThisPtr(), 4.0f));
	shared_ptr<Shape> bullet_shape = make_shared<Shape>("bullet.shape");
	mSpaceship->SetBulletShape(bullet_shape);
	Animation *anim_ptr = AnimationManager::GetInstance().GetAnimationByName("spaceship");
	shared_ptr<Sprite> spaceship_sprite =
		make_shared<Sprite>(anim_ptr->GetWidth(), anim_ptr->GetHeight(), anim_ptr);
	mSpaceship->SetSprite(spaceship_sprite);
	mSpaceship->SetScale(0.1f);
	// Reset spaceship back to centre of the world
	mSpaceship->Reset();
	// Return the spaceship so it can be added to the world
	return mSpaceship;

}

void Asteroids::CreateAsteroids(const uint num_asteroids, AsteroidSize size, GLVector3f* position)
{
	mObjectiveCount += num_asteroids;
	for (uint i = 0; i < num_asteroids; i++)
	{
		Animation *anim_ptr = AnimationManager::GetInstance().GetAnimationByName("asteroid1");
		shared_ptr<Sprite> asteroid_sprite
			= make_shared<Sprite>(anim_ptr->GetWidth(), anim_ptr->GetHeight(), anim_ptr);
		asteroid_sprite->SetLoopAnimation(true);
		shared_ptr<GameObject> asteroid = make_shared<Asteroid>(size);
		asteroid->SetBoundingShape(make_shared<BoundingSphere>(asteroid->GetThisPtr(), 10.0f/size)); //Note AsteroidSize implicity casts to Int
		asteroid->SetSprite(asteroid_sprite);
		asteroid->SetScale(0.2f/size); //Note AsteroidSize implicity casts to Int
		if(position != nullptr) asteroid->SetPosition(*position); //We used original position asteroid to imitate breaking
		mGameWorld->AddObject(asteroid);
	}
}


void Asteroids::CreateGUI()
{

	// Add a (transparent) border around the edge of the game display
	mGameDisplay->GetContainer()->SetBorder(GLVector2i(10, 10));
	// Create a new GUILabel and wrap it up in a shared_ptr
	mScoreLabel = make_shared<GUILabel>("Score: 0");
	// Set the vertical alignment of the label to GUI_VALIGN_TOP
	mScoreLabel->SetVerticalAlignment(GUIComponent::GUI_VALIGN_TOP);
	// Add the GUILabel to the GUIComponent  
	shared_ptr<GUIComponent> score_component
		= static_pointer_cast<GUIComponent>(mScoreLabel);
	mGameDisplay->GetContainer()->AddComponent(score_component, GLVector2f(0.0f, 1.0f));

	// Create a new GUILabel and wrap it up in a shared_ptr
	mLivesLabel = make_shared<GUILabel>("Lives: 3");
	// Set the vertical alignment of the label to GUI_VALIGN_BOTTOM
	mLivesLabel->SetVerticalAlignment(GUIComponent::GUI_VALIGN_BOTTOM);
	// Add the GUILabel to the GUIComponent  
	shared_ptr<GUIComponent> lives_component = static_pointer_cast<GUIComponent>(mLivesLabel);
	mGameDisplay->GetContainer()->AddComponent(lives_component, GLVector2f(0.0f, 0.0f));

	// Create a new GUILabel and wrap it up in a shared_ptr
	mGameOverLabel = make_shared<GUILabel>("GAME OVER!");
	// Set the horizontal alignment of the label to GUI_HALIGN_CENTER
	mGameOverLabel->SetHorizontalAlignment(GUIComponent::GUI_HALIGN_CENTER);
	// Set the vertical alignment of the label to GUI_VALIGN_MIDDLE
	mGameOverLabel->SetVerticalAlignment(GUIComponent::GUI_VALIGN_MIDDLE);
	// Set the visibility of the label to false (hidden)
	mGameOverLabel->SetVisible(false);
	// Add the GUILabel to the GUIContainer  
	shared_ptr<GUIComponent> game_over_component
		= static_pointer_cast<GUIComponent>(mGameOverLabel);
	mGameDisplay->GetContainer()->AddComponent(game_over_component, GLVector2f(0.5f, 0.5f));

	mCongratsLabel = std::make_shared<GUILabel>("New highscore, please enter your name");
	// Set the horizontal alignment of the label to GUI_HALIGN_CENTER
	mCongratsLabel->SetHorizontalAlignment(GUIComponent::GUI_HALIGN_CENTER);
	// Set the vertical alignment of the label to GUI_VALIGN_MIDDLE
	mCongratsLabel->SetVerticalAlignment(GUIComponent::GUI_VALIGN_MIDDLE);
	// Set the visibility of the label to false (hidden)
	mCongratsLabel->SetVisible(false);
	// Add the GUILabel to the GUIContainer  
	shared_ptr<GUIComponent> congrats_component
		= static_pointer_cast<GUIComponent>(mCongratsLabel);
	mGameDisplay->GetContainer()->AddComponent(congrats_component, GLVector2f(0.5f, 0.44f));

	// Create a new GUILabel and wrap it up in a shared_ptr
	mNameLabel = make_shared<GUILabel>("");
	// Set the vertical alignment of the label to GUI_VALIGN_MIDDLE
	mNameLabel->SetVerticalAlignment(GUIComponent::GUI_VALIGN_MIDDLE);
	// Set the vertical alignment of the label to GUI_VALIGN_MIDDLE
	mNameLabel->SetHorizontalAlignment(GUIComponent::GUI_HALIGN_CENTER);
	// Set the visibility of the label to false (hidden)
	mNameLabel->SetVisible(false);
	// Add the GUILabel to the GUIComponent  
	shared_ptr<GUIComponent> nameComponent = static_pointer_cast<GUIComponent>(mNameLabel);
	mGameDisplay->GetContainer()->AddComponent(mNameLabel, GLVector2f(0.5f, 0.38f));

	// Create a new GUILabel and wrap it up in a shared_ptr
	mLeaderboard = make_shared<GUILabel>(mHighscores.GetAllHighscores());
	// Set the horizontal alignment of the label to GUI_HALIGN_CENTER
	mLeaderboard->SetHorizontalAlignment(GUIComponent::GUI_HALIGN_CENTER);
	// Set the vertical alignment of the label to GUI_VALIGN_MIDDLE
	mLeaderboard->SetVerticalAlignment(GUIComponent::GUI_VALIGN_MIDDLE);
	// Set the visibility of the label to false (hidden)
	mLeaderboard->SetVisible(false);
	// Add the GUILabel to the GUIContainer  
	shared_ptr<GUIComponent> leaderboard_component
		= static_pointer_cast<GUIComponent>(mLeaderboard);
	mGameDisplay->GetContainer()->AddComponent(leaderboard_component, GLVector2f(0.5f, 0.5f));

}

void Asteroids::OnScoreChanged(int score)
{
	// Format the score message using an string-based stream
	std::ostringstream msg_stream;
	msg_stream << "Score: " << score;
	// Get the score message as a string
	std::string score_msg = msg_stream.str();
	mScoreLabel->SetText(score_msg);
}

void Asteroids::OnPlayerKilled(int lives_left)
{
	shared_ptr<GameObject> explosion = CreateExplosion(0.5f);
	explosion->SetPosition(mSpaceship->GetPosition());
	explosion->SetRotation(mSpaceship->GetRotation());
	mGameWorld->AddObject(explosion);

	// Format the lives left message using an string-based stream
	std::ostringstream msg_stream;
	msg_stream << "Lives: " << lives_left;
	// Get the lives left message as a string
	std::string lives_msg = msg_stream.str();
	mLivesLabel->SetText(lives_msg);

	if (lives_left > 0) 
	{ 
		SetTimer(1000, CREATE_NEW_PLAYER); 
	}
	else
	{
		SetTimer(500, SHOW_GAME_OVER);
	}
}

shared_ptr<GameObject> Asteroids::CreateExplosion(float scale)
{
	Animation *anim_ptr = AnimationManager::GetInstance().GetAnimationByName("explosion");
	shared_ptr<Sprite> explosion_sprite =
		make_shared<Sprite>(anim_ptr->GetWidth(), anim_ptr->GetHeight(), anim_ptr);
	explosion_sprite->SetLoopAnimation(false);
	shared_ptr<GameObject> explosion = make_shared<Explosion>();
	explosion->SetSprite(explosion_sprite);
	explosion->Reset();
	explosion->SetScale(scale);
	return explosion;
}

void Asteroids::CreateEnemyship()
{
	if(mAlien == nullptr){
		mObjectiveCount += 1;
		Animation *anim_ptr = AnimationManager::GetInstance().GetAnimationByName("enemy");
		shared_ptr<Sprite> enemy_sprite
			= make_shared<Sprite>(anim_ptr->GetWidth(), anim_ptr->GetHeight(), anim_ptr);
		enemy_sprite->SetLoopAnimation(true);
		mAlien = make_shared<EnemyShip>(mSpaceship);
		mAlien->SetBoundingShape(make_shared<BoundingSphere>(mAlien->GetThisPtr(), 6.0f));
		shared_ptr<Shape> bullet_shape = make_shared<Shape>("enemybullet.shape");
		mAlien->SetBulletShape(bullet_shape);
		mAlien->SetSprite(enemy_sprite);
		mAlien->SetScale(0.1f);
		mGameWorld->AddObject(mAlien);
		SetTimer(1000, ATTACK_PLAYER);
		SetTimer(100, DODGE_PLAYER);
	}else
	{
		mObjectiveCount += 1;
		mAlien->Respawn(); //Reset Enemy Spaceship object, and place it in a random location
		mGameWorld->AddObject(mAlien);
	}
}




