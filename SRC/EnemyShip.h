#ifndef __ENEMYSHIP_H__
#define __ENEMYSHIP_H__

#include <unordered_map>
#include "GameUtil.h"
#include "GameObject.h"
#include "Shape.h"
#include "Spaceship.h"


class EnemyShip : public GameObject, public IGameWorldListener
{
public:
	EnemyShip(shared_ptr<Spaceship> player);
	EnemyShip(GLVector3f p, GLVector3f v, GLVector3f a, GLfloat h, GLfloat r, shared_ptr<Spaceship> player);
	EnemyShip(const EnemyShip& s, shared_ptr<Spaceship> player);
	virtual ~EnemyShip(void);

	// Declaration of IGameWorldListener interface //////////////////////////////

	void OnWorldUpdated(GameWorld* world) {}
	void OnObjectAdded(GameWorld* world, shared_ptr<GameObject> object);
	void OnObjectRemoved(GameWorld* world, shared_ptr<GameObject> object);


	virtual void Update(int t);
	virtual void Render(void);

	virtual void Thrust(float t);
	virtual void Rotate(float r);
	virtual void Shoot(void);

	void SetBulletShape(shared_ptr<Shape> bullet_shape) { mBulletShape = bullet_shape; }

	bool CollisionTest(shared_ptr<GameObject> o) override;
	void OnCollision(const GameObjectList &objects) override;

	void CalculateDodge();
	bool IsDodging() const;
	void Respawn();

private:
	float mThrust;
	bool mIsDodging;

	shared_ptr<Shape> mBulletShape;
	const shared_ptr<Spaceship> mPlayerSpaceship;
	unordered_map<size_t, shared_ptr<GameObject>> mPlayerBullets;


	void updateEnemyAngle();
	void updateEnemyMovement();
};

#endif