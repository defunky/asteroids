#include "GameUtil.h"
#include "GameWorld.h"
#include "EnemyBullet.h"
#include "EnemyShip.h"
#include "BoundingSphere.h"

using namespace std;

// PUBLIC INSTANCE CONSTRUCTORS ///////////////////////////////////////////////

/**  Default constructor. */
EnemyShip::EnemyShip(shared_ptr<Spaceship> player)
	: GameObject("EnemyShip"), mThrust(0), mPlayerSpaceship(player), mIsDodging(false)
{
	mPosition.x = rand() / 2;
	mPosition.y = rand() / 2;
}

/** Construct a EnemyShip with given position, velocity, acceleration, angle, and rotation. */
EnemyShip::EnemyShip(GLVector3f p, GLVector3f v, GLVector3f a, GLfloat h, GLfloat r, shared_ptr<Spaceship> player)
	: GameObject("EnemyShip", p, v, a, h, r), mThrust(0), mPlayerSpaceship(player), mIsDodging(false)
{
}

/** Copy constructor. */
EnemyShip::EnemyShip(const EnemyShip& s, shared_ptr<Spaceship> player)
	: GameObject(s), mThrust(0), mPlayerSpaceship(player), mIsDodging(false)
{
}

/** Destructor. */
EnemyShip::~EnemyShip(void)
{
}

void EnemyShip::OnObjectAdded(GameWorld* world, shared_ptr<GameObject> object)
{
	if (object->GetType() == GameObjectType("Bullet"))
	{
		//Every 'alive' bullet created we want to know about.
		//We create a hash of object pointer to get unique id to place in a map.
		std::hash<GameObject*> hasher;
		size_t hash = hasher(object.get());
		mPlayerBullets.insert({ hash, object });
	}
}

void EnemyShip::OnObjectRemoved(GameWorld* world, shared_ptr<GameObject> object)
{
	if (object->GetType() == GameObjectType("Bullet"))
	{
		//If a bullet timesout, or collides we remove it from the map
		//as we no longer need it anymore, hashing object pointer will give us same unique id
		//we used to insert it.
		std::hash<GameObject*> hasher;
		size_t hash = hasher(object.get());
		mPlayerBullets.erase(hash);
	}
}

// PUBLIC INSTANCE METHODS ////////////////////////////////////////////////////

void EnemyShip::updateEnemyAngle()
{
	auto angle = std::atan2(mPlayerSpaceship->GetPosition().y - GetPosition().y, mPlayerSpaceship->GetPosition().x - GetPosition().x);
	angle = angle * (180 / 3.14); //Convert to degrees
	SetAngle(angle);
}

void EnemyShip::updateEnemyMovement()
{
	//Calculate the distance between the Player ship and the enemy ship
	auto distance = std::sqrt(std::pow(mPlayerSpaceship->GetPosition().x - GetPosition().x, 2) +
		std::pow(mPlayerSpaceship->GetPosition().y - GetPosition().y, 2));

	//Try to move back if too close to the player to avoid collision (this won't help if player/enemy going too fast)
	if (distance > 45)
	{
		Thrust(4);
	}
	else
	{
		Thrust(-4);
	}

	//Speed limit, we don't want it to go super-fast
	if (GetVelocity().x > 10) SetVelocity(GLVector3f(10, GetVelocity().y, GetVelocity().z));
	if (GetVelocity().x < -10) SetVelocity(GLVector3f(-10, GetVelocity().y, GetVelocity().z));
	if (GetVelocity().y > 10) SetVelocity(GLVector3f(GetVelocity().x, 10, GetVelocity().z));
	if (GetVelocity().y < -10) SetVelocity(GLVector3f(GetVelocity().x, -10, GetVelocity().z));

}

/** Update this EnemyShip. */
void EnemyShip::Update(int t)
{
	// Call parent update function
	GameObject::Update(t);
	//If it's currently dodging we don't need to aim at player or move towards the player
	if(!mIsDodging){
		//Calculate angle between enemy ship and player
		updateEnemyAngle();
		//Calculate distance between enemy and player move accordingly
		updateEnemyMovement();
	}
}

/** Render this EnemyShip. */
void EnemyShip::Render(void)
{
	GameObject::Render();
}

/** Fire the rockets. */
void EnemyShip::Thrust(float t)
{
	mThrust = t;
	// Increase acceleration in the direction of ship
	mAcceleration.x = mThrust*cos(DEG2RAD*mAngle);
	mAcceleration.y = mThrust*sin(DEG2RAD*mAngle);
}

/** Set the rotation. */
void EnemyShip::Rotate(float r)
{
	mRotation = r;
}

/** Shoot a bullet. */
void EnemyShip::Shoot(void)
{
	// Check the world exists
	if (!mWorld) return;
	// Construct a unit length vector in the direction the EnemyShip is headed
	GLVector3f spaceship_heading(cos(DEG2RAD*mAngle), sin(DEG2RAD*mAngle), 0);
	spaceship_heading.normalize();
	// Calculate the point at the node of the EnemyShip from position and heading
	GLVector3f bullet_position = mPosition + (spaceship_heading * 4);
	// Calculate how fast the bullet should travel
	float bullet_speed = 20;
	// Construct a vector for the bullet's velocity
	GLVector3f bullet_velocity = mVelocity + spaceship_heading * bullet_speed;
	// Construct a new bullet
	shared_ptr<GameObject> bullet
	(new EnemyBullet(bullet_position, bullet_velocity, mAcceleration, mAngle, 0, 2000));
	bullet->SetBoundingShape(make_shared<BoundingSphere>(bullet->GetThisPtr(), 2.0f));
	bullet->SetShape(mBulletShape);
	// Add the new bullet to the game world
	mWorld->AddObject(bullet);

}

bool EnemyShip::CollisionTest(shared_ptr<GameObject> o)
{
	if (o->GetType() != GameObjectType("Spaceship") && o->GetType() != GameObjectType("Bullet")) return false;
	if (mBoundingShape.get() == NULL) return false;
	if (o->GetBoundingShape().get() == NULL) return false;
	return mBoundingShape->CollisionTest(o->GetBoundingShape());
}

void EnemyShip::OnCollision(const GameObjectList &objects)
{
	mWorld->FlagForRemoval(GetThisPtr());
}

void EnemyShip::CalculateDodge()
{
	mIsDodging = false;
	//Calculate the distance of each 'alive' bullet at this time
	for (auto bullet : mPlayerBullets) {
		auto distance = std::sqrt(std::pow(bullet.second->GetPosition().x - GetPosition().x, 2) +
			std::pow(bullet.second->GetPosition().y - GetPosition().y, 2));
		//If the distance between a bullet and enemy is within a certain range, try to dodge
		if (distance < 35) {
			mIsDodging = true;
			break;
		}
	}
}

bool EnemyShip::IsDodging() const
{
	return mIsDodging;
}

void EnemyShip::Respawn()
{
	mPosition.x = rand() / 2;
	mPosition.y = rand() / 2;
	SetVelocity(GLVector3f(0, 0, 0));
	SetAcceleration(GLVector3f(0, 0, 0));
	SetAngle(0);
	SetRotation(0);
}
