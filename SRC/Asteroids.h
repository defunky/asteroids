#ifndef __ASTEROIDS_H__
#define __ASTEROIDS_H__

#include "GameUtil.h"
#include "GameSession.h"
#include "IKeyboardListener.h"
#include "IGameWorldListener.h"
#include "IScoreListener.h" 
#include "ScoreKeeper.h"
#include "Player.h"
#include "IPlayerListener.h"
#include "Highscores.h"
#include "EnemyShip.h"
#include "Asteroid.h"

class GameObject;
class Spaceship;
class GUILabel;

class Asteroids : public GameSession, public IKeyboardListener, public IGameWorldListener, public IScoreListener, public IPlayerListener
{
public:
	Asteroids(int argc, char *argv[]);
	virtual ~Asteroids(void);

	virtual void Start(void);
	virtual void Stop(void);

	// Declaration of IKeyboardListener interface ////////////////////////////////

	void OnKeyPressed(uchar key, int x, int y);
	void InGameInput(uchar key, int x, int y);
	void LeaderboardNameInput(uchar key, int x, int y);
	void OnKeyReleased(uchar key, int x, int y);
	void OnSpecialKeyPressed(int key, int x, int y);
	void OnSpecialKeyReleased(int key, int x, int y);

	// Declaration of IScoreListener interface //////////////////////////////////

	void OnScoreChanged(int score);

	// Declaration of the IPlayerLister interface //////////////////////////////

	void OnPlayerKilled(int lives_left);

	// Declaration of IGameWorldListener interface //////////////////////////////

	void OnWorldUpdated(GameWorld* world) {}
	void OnObjectAdded(GameWorld* world, shared_ptr<GameObject> object) {}
	void OnObjectRemoved(GameWorld* world, shared_ptr<GameObject> object);

	// Override the default implementation of ITimerListener ////////////////////
	void OnTimer(int value);

private:
	shared_ptr<Spaceship> mSpaceship;
	shared_ptr<GUILabel> mScoreLabel;
	shared_ptr<GUILabel> mLivesLabel;
	shared_ptr<GUILabel> mGameOverLabel;
	shared_ptr<GUILabel> mNameLabel;
	shared_ptr<GUILabel> mCongratsLabel;
	shared_ptr<GUILabel> mLeaderboard;
	shared_ptr<EnemyShip> mAlien;

	uint mLevel;
	uint mObjectiveCount;
	Highscores mHighscores;
	bool mIsTypingName;


	void ResetSpaceship();
	shared_ptr<GameObject> CreateSpaceship();
	void CreateAsteroids(const uint num_asteroids, AsteroidSize size = Regular, GLVector3f* position = nullptr);
	void CreateGUI();
	shared_ptr<GameObject> CreateExplosion(float scale);
	void CreateEnemyship();
	
	const static uint SHOW_GAME_OVER = 0;
	const static uint START_NEXT_LEVEL = 1;
	const static uint CREATE_NEW_PLAYER = 2;
	const static uint SHOW_LEADERBOARD = 3;
	const static uint ATTACK_PLAYER = 4;
	const static uint DODGE_PLAYER = 5;

	ScoreKeeper mScoreKeeper;
	Player mPlayer;
};

#endif