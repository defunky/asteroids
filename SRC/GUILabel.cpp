#include <string>
#include "GUILabel.h"

// PUBLIC INSTANCE CONSTRUCTORS ///////////////////////////////////////////////

/** Default constructor. */
GUILabel::GUILabel() : mText(""), mFontWidth(9), mFontHeight(15)
{
}

/** Construct label with given text. */
GUILabel::GUILabel(const string& text) : mText(text), mFontWidth(9), mFontHeight(15)
{
}

/** Destructor. */
GUILabel::~GUILabel()
{
}

// PUBLIC INSTANCE METHODS ////////////////////////////////////////////////////

/** Draw label by drawing text with a fixed font. */
void GUILabel::Draw()
{
	if (!mVisible) return;

	int w = CalculateWidth();
	int h = CalculateHeight();

	int align_x = 0;
	int align_y = 0;
	if (mHorizontalAlignment == GUIComponent::GUI_HALIGN_RIGHT) {
		align_x = -w;
	} else if (mHorizontalAlignment == GUIComponent::GUI_HALIGN_CENTER) {
		align_x = -w/2;
	}

	if (mVerticalAlignment == GUIComponent::GUI_VALIGN_TOP) {
		align_y = -h;
	} else if (mVerticalAlignment == GUIComponent::GUI_VALIGN_MIDDLE) {
		align_y = -h/2;
	}

	glDisable(GL_LIGHTING);
	glColor3f(mColor[0], mColor[1], mColor[2]);
	glRasterPos2i(mPosition.x + mBorder.x + align_x, mPosition.y + mBorder.y + align_y);

	uint newLineCount = 0;
	for (uint i = 0; i < mText.length(); ++i) {
		if (mText[i] == '\n'){
			newLineCount++;
			glRasterPos2i(mPosition.x + mBorder.x + align_x, mPosition.y + mBorder.y + align_y + (newLineCount * NEW_LINE_SPACING));
		}
		else{
			glutBitmapCharacter(GLUT_BITMAP_9_BY_15, mText[i]);
		}
	}
	glEnable(GL_LIGHTING);
}

int GUILabel::CalculateWidth()
{
	std::size_t newLinePos = mText.find_first_of('\n'); //Find the first newline char

	if (newLinePos == std::string::npos) { //If there isn't one, calculate width normally
		return (int)(mText.length() * mFontWidth);
	}

	//If we find one, calculate the length of first sentence only.
	return (int)(mText.substr(0, newLinePos).length()) * mFontWidth; 
}

int GUILabel::CalculateHeight()
{
	std::size_t amountOfLines = std::count(mText.begin(), mText.end(), '\n');
	if(amountOfLines == 0) //If there's no new lines, it a single sentence.
	{
		return mFontHeight;
	}

	//If there are multiple lines, take in consideration the spacing alongside the height of font.
	return amountOfLines * NEW_LINE_SPACING + mFontHeight;
}