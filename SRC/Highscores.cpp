#include "Highscores.h"
#include <iostream>
#include <fstream>
#include <string>
#include <algorithm>


Highscores::Highscores() : mFile(HS_FILE)
{
	mHighscores.reserve(10); //We reserve space for highscores to prevent size reallocation
	InitalizeHighscoreFile();
}

void Highscores::InitalizeHighscoreFile()
{
	if (!mFile.good()) //File doesn't exist or is corrupted
	{
		std::ofstream outfile(HS_FILE);
		//Add random 10 scores
		outfile << "Please " << 10000 << '\n';
		outfile << "Give " << 7500 << '\n';
		outfile << "Me " << 5000 << '\n';
		outfile << "First " << 4410 << '\n';
		outfile << "IceCube " << 3000 << '\n';
		outfile << "Yeezy " << 2500 << '\n';
		outfile << "Snoop " << 2000 << '\n';
		outfile << "Dogg " << 1210 << '\n';
		outfile << "DrDre " << 60 << '\n';
		outfile << "AAA " << 10;
		outfile.close();

		mFile = std::ifstream(HS_FILE); //Re-open the file
	}

	std::string hsName;
	int hsScore;
	int counter = 0; 
	//Only read up to 10th line in file, as someone might of modified to have more than limit (10)
	while ((mFile >> hsName >> hsScore) && counter < HIGHSCORE_LIMIT) 
	{
		counter++;
		mHighscores.push_back(std::tuple<std::string, int> (hsName, hsScore));
	}
}

void Highscores::AddNewHighscore(std::string playerName, int highscore)
{
	mHighscores.pop_back(); //Remove the lowest score to make way for newest highscore
	mHighscores.push_back(std::tuple<std::string, int>(playerName, highscore)); //Add the new score

	//Sort the vector going from highest to lowest score. i.e last element ([9]) is lowest score
	std::sort(mHighscores.begin(), mHighscores.end(), 
		[](const std::tuple<std::string, int>& player, const std::tuple<std::string, int>& otherPlayer) -> bool
	{
		return std::get<1>(player) > std::get<1>(otherPlayer);
	});

	SaveHighscores();
}

void Highscores::SaveHighscores()
{
	std::ofstream outfile(HS_FILE, std::ios::out | std::ios::trunc); //Empty the text file to write our new highscore list
	for (int i = 0; i < mHighscores.size(); ++i)
	{
		outfile << std::get<0>(mHighscores[i]) << ' ' << std::get<1>(mHighscores[i]);
		//If it's not last highscore append it with newline
		if (i != mHighscores.size() - 1) outfile << '\n';
	}

	outfile.close();

}

std::string Highscores::GetAllHighscores(){
	std::string allHighscores;
	for (int i = 0; i < mHighscores.size(); ++i)
	{
		std::string name = std::get<0>(mHighscores[i]);
		std::string score = std::to_string(std::get<1>(mHighscores[i]));

		allHighscores += name + std::string(20 - (name.length() + score.length()), '.') + score;

		//If it's not last highscore append it with newline
		if (i != mHighscores.size() - 1) allHighscores += '\n';

	}
	return allHighscores;
}

bool Highscores::IsCurrentScoreHighscore(int score)
{
	bool isHighscore = false;
	for (int i = 0; i < mHighscores.size(); ++i)
	{
		isHighscore = std::get<1>(mHighscores[i]) < score;
	}

	return isHighscore;

}