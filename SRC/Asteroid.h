#ifndef __ASTEROID_H__
#define __ASTEROID_H__

#include "GameObject.h"

enum AsteroidSize
{
	//These enums are used to scale the asteroid size & explosion,
	//If you plan on adding more ensure that then increment positively with no break
	//The larger the number the smaller the asteroid will be scaled.
	Regular = 1,
	Small = 2,
	Tiny = 3
};

class Asteroid : public GameObject
{
public:
	Asteroid(AsteroidSize size);
	~Asteroid(void);

	bool CollisionTest(shared_ptr<GameObject> o);
	void OnCollision(const GameObjectList& objects);
	AsteroidSize GetCurrentSize() const;

private:
	AsteroidSize mSize;
};

#endif
